# Deploy Openshift Custom Certificates

This role deploy the certificates on your openshift cluster.
 
**Make sure to run the playbook generate-custom-certificates-ocp4 for genarate the certificate files**

## add to you inventory the following parameters:
--------------------------------------------------------------------------------

* **cluster_name:** your cluster name
* **ocp_web:** our apache server fqdn(snir-installer-rhel.centralus.cloudapp.azure.com)
* **openshift_custom_certificates:** true